class SessionsController < ApplicationController
  def new

  end

	def create
	  	username = params[:session] [:username]
	  	password = params[:session] [:password]
	  	# :session comes from the name of the form!
	  	# digging into the nested hash (session is one key and username is the other)
	  	#not instance variables as we don't want them going to the view!
	  	@user = User.find_by(username: username)
	  	#look inside the username column for a user name that matches @username
	  	# .authenticate method comes from bcrypt
	  	if @user.present? and @user.authenticate(password)
	  		flash[:success] = "Logged in!"
	  		session[:user_id] = @user.id
	  		redirect_to root_path
	  	else
	  		flash[:error] = "Something went wrong"
	  		render :new
	  	end
	end

    def show

    end

    def destroy
    	reset_session
    	flash[:success] = "Logged out"
    	redirect_to root_path
    end

end
