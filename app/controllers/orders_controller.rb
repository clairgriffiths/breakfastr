class OrdersController < ApplicationController



	def new
	  	@pastry = Pastry.find(params[:pastry_id])
	  	@order = @pastry.orders.new
	end

	def create
		@pastry = Pastry.find(params[:pastry_id])
		@order = @pastry.orders.new(order_params)
		@order.user = current_user

			if @order.save
	     	flash[:success] = "Thanks for ordering!"
	     	logger.debug @order

	     	#logger helps with debugging, adds everything to the development.log file. Not the greatest resource
	    	redirect_to order_path(@order)

	    	# should be redirecting to the 'show' page (or action in this case)
	    	# Try to charge card here

	    	Stripe.api_key = Rails.application.secrets.stripe_secret_key

	    	Stripe::Charge.create(
	    		:amount => @pastry.price_in_pence,
	    		:currency => "gbp",
	    		:source => @order.stripe_token, #obtained with stripe.js
	    		:description => "Charge for #{@order.user.email}")
	    	

	    	# Copy the Stripe.api_key = part but after moving the api key into the secrets folder, will then need to reference that instead 
	    	# copy the key into config/secrets

	    	else
	     	flash[:error] = "Oops, that didn't work (order/payment)"
	     	render :new
	     	end
	end	



  
	def show
	end

private

	def order_params
		params.require(:order).permit(:stripe_token)
	end



end
