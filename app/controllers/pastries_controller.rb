class PastriesController < ApplicationController

  before_action :find_pastry, except: [:index, :destroy, :create, :new]
  before_action :require_user, except: [:index, :show]
  before_action :require_owner, only: [:edit, :update, :destroy]

  def index
  	@pastries = Pastry.all
  end
  	 
# hash can be written using the hash rocket or colon 

  def show
  	#@pastry = Pastry.find(params[:id])
  end

  def new
    @pastry = Pastry.new
  end

  def create

    @pastry = current_user.pastries.new(pastry_params)
    if @pastry.save
      flash[:success] = "Pastry added!"
      redirect_to root_path
    else
      flash[:error] = "Oops, something went wrong, please try again"
      render :new
    end
  end

#can render any of the views from the Pastry controller just by doing this


  def edit
    #@pastry = Pastry.find(params[:id])
    #require_owner      
  end

  def update
    #@pastry = Pastry.find(params[:id])
      if @pastry.update(pastry_params)
        flash[:success] = "Pastry updated!"
        redirect_to pastry_path(@pastry)
      else
        flash[:error] = "Oops, that didn't save, please try again"
        render :edit
      end
  end

  def destroy
    pastry = Pastry.find(params[:id])
    pastry.destroy 
    flash[:success] = "You have deleted your #{pastry.name}"
    redirect_to root_path
  end
  # Also not an instance variable because it isn't being passed through to a view
  # The reason its not an instance variable is that, if there were a view, that variable no longer exists! (i.e. it has just been deleted so what are they going to call)
 
  private

  def pastry_params
    params.require(:pastry).permit(:name, :price_in_pence, :description, :image)
  end
# Need to 'whitelist' the params - i.e. allow ONLY the params from the params hash (created from the http request) to be passed into the create method 

def find_pastry
  @pastry = Pastry.find(params[:id])
end


end


