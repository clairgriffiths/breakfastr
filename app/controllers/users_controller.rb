class UsersController < ApplicationController
	def new
		@user = User.new
	end

	def create
	  	@user = User.new(user_params)
	  	
	  	if @user.save
	  		#log them in here
	  		#security thing, reset the session before starting another one. Reset_session also just comes with Rails
	  		reset_session
	  		session[:user_id] = @user.id
	  		#session is a keyword given to us by rails - behaves like a hash
	  		# user_id is a key within the session hash
			flash[:success] = "You have created a new user"
			redirect_to root_path
		else
			flash[:error] = "Something went wrong"
			render :new
		end
	end

	def edit
		@user = User.find(params[:id])
	end

	def update
		@user = User.find(params[:id])
		if @user.update(user_params)
			flash[:success] = "You have updated your details"
			redirect_to root_path
		else
			flash[:error] = "Something went wrong"
			render :edit
		end
	end

	def destroy
		@user = User.find(params[:id])
		@user.destroy
		flash[:success] = "User destroyed"
		redirect_to root_path
	end

private
	def user_params
		params.require(:user).permit(:email, :password, :password_confirmation, :username)
	end

end

