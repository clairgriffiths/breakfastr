class Pastry < ActiveRecord::Base
	belongs_to :user
	has_many :orders

	has_attached_file :image, :styles => { :medium => "300x300>", :thumb => "100x100>" }, :default_url => "/images/:style/missing.png"
  	validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/
  	#taken from paperclip github page

	validates :name, presence: true, uniqueness: true
	validates :price_in_pence, numericality: {greater_than: 50}
end

#changed avatar to image (as tyhat is what we're going to be using in the migration)