class User < ActiveRecord::Base
	has_secure_password
	has_many :pastries
	has_many :orders

	validates :email, presence: true, uniqueness: true
	validates :username, presence: true, uniqueness:true

	def owns(pastry)
		
		pastry.user == self
			
		
		#check if owner of pastry
		#return true if true

	# can't access the session within the model so can't do pastry.user_id == session.user_id
		
	end

end

#references the Bcrypt gem to encrypt the password

#have to have password_digest in the model (i.e. in the database table)
#have to have password and password_corfirmation in whitelisted params (user_params)
