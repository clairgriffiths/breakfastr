Rails.application.routes.draw do
  
    root 'pastries#index'

    resources :pastries do
    	resources :orders, only: [:new, :create, :show]
    end

    resources :users, except: [:show, :index] do
    	resources :pastries, only: [:index]
    end

    resources :orders, only: :show

    #This is a custom route to save having to use the users part of it (since you have to be logged in to do this)

    resource :session
	end

#Means that you need to go to/users/1/pastries/3 (i.e. creating a new pastry is within the user)
# User info lives in a session, so we don't actually need to go via a user to get to the pastries. We can just use the current session
#nested resources - probably want to look up