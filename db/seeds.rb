# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)



# hash cam be written using the hash rocket or colon


Pastry.create([
  		{name: "Croissant", price_in_pence: 350, description: "A moon shaped pastry"},
  		{name: "Pain au chocolat", price_in_pence: 250, description: "Bread with Chocolate"}
  		])