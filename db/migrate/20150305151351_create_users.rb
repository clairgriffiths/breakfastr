class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
		t.string :email
    	t.string :password_digest
    	t.string :username
    	t.timestamps
    end
  end
end

#has to be password_digest as thats what the gem looks like