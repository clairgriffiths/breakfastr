class AddUserIdToPastries < ActiveRecord::Migration
  def change
  	add_column :pastries, :user_id, :integer
  	add_index :pastries, :user_id
  end
end

#add_column needs table you are adding it to, name of column you are adding, datatype
#add_index speeds up foreign key lookups